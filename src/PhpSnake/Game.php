<?php

declare (strict_types = 1);

namespace PhpSnake;

use PhpSnake\Exception\GameException;
use PhpSnake\Game\Board;
use PhpSnake\Game\Drawer;

class Game
{
    /**
     * @var Terminal
     */
    private $terminal;

    /**
     * @var Board
     */
    private $board;

    /**
     * @var Drawer
     */
    private $drawer;

    /**
     * @return void
     */
    public function __construct()
    {
        $this->terminal = new Terminal();
        $this->board = new Board(
            intval($this->terminal->getWidth() * 1),
            intval($this->terminal->getHeight() - 3)
        );
        $this->drawer = new Drawer(STDOUT);

        $this->drawBoard();
    }

    /**
     * @return void
     */
    public function run()
    {
        try {
            while (true) {
                $input = $this->terminal->getChar();
                $this->board->moveSnake($input);
                $this->drawBoard();
                usleep( $this->board->snake->getSpeed() );
            }
        } catch (GameException $exception) {
            $this->gameOver();
        }
    }

    /**
     * @return void
     */
    public function gameOver()
    {
        $this->board->writeGameOver();
        $this->drawBoard();
    }

    /**
     * @return void
     */
    private function drawBoard()
    {
        $this->drawer->draw($this->board, $this->board->snake);
    }
}
