<?php

declare (strict_types = 1);

namespace PhpSnake\Game;

class Drawer
{
    /**
     * @var string
     */
    private $stream;

    /**
     * @param resource $stream
     */
    public function __construct($stream)
    {
        $this->stream = $stream;
    }

    /**
     * @param Board $board
     */
    public function draw(Board $board, Snake $snake)
    {
        $this->hideCursor();
        $this->moveCursorToStart();

        $this->newLine();

        fwrite( $this->stream, "Score: {$snake->getScore()}" );
        fwrite( $this->stream, "\t" );
        fwrite( $this->stream, "Level: {$snake->getLevel()}" );
        fwrite( $this->stream, "\n" );

        foreach ($board->getMap() as $line) {
            foreach ($line as $char) {
                fwrite($this->stream, $char);
            }
            $this->newLine();
        }

        $this->showCursor();
    }

    /**
     * @return void
     */
    private function newLine()
    {
        fwrite($this->stream, PHP_EOL);
    }

    /**
     * @return void
     */
    private function moveCursorToStart()
    {
        fwrite($this->stream, "\033[0;0f");
    }

    /**
     * @return void
     */
    private function hideCursor()
    {
        fwrite($this->stream, "\033[?25l");
    }

    /**
     * @return void
     */
    private function showCursor()
    {
        fwrite($this->stream, "\033[?25h\033[?0c");
    }
}
