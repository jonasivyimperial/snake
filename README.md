# Snake Game
## Launch

Run simple command in CLI:

```
php snake.php
```

Keys:

```
w - move up
a - move left
s - move down
d - move right
```